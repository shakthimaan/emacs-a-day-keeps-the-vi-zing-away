TARGET=emacs-a-day-keeps-the-vi-zing-away
DIRED=dired-ref
REFCARD=refcard
ORG=orgcard

all: dired vizing orgcard

vizing:
	pdftex $(TARGET).tex

orgcard:
	pdftex $(ORG).tex

dired:
	tex $(DIRED).tex
	dvips -t a4 $(DIRED).dvi
	ps2pdf $(DIRED).ps

clean:
	rm -f *~ *.ps *.pdf *.log *.out *.aux *.dvi